sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Detail", {
		onInit: function(oEvent) {
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			this._oRouter.attachRouteMatched(function(oEvent) {
				var requisitionId = oEvent.getParameter("arguments").requisitionId;
				$.ajax("/Flow7Api/api/fdp/m/" + requisitionId)
					.done(function(data) {
						var oData = new sap.ui.model.json.JSONModel(data);
						self.getView().setModel(oData, "formDetail");
					});
			});
		}
	});
});