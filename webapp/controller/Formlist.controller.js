sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Formlist", {
		onInit: function() {
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			$.ajax("/Flow7Api/api/dashboard/processing/folder")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					self.getOwnerComponent().setModel(oData, "lists");
				});
			this._oRouter.getRoute("formlist").attachMatched(this._handleRouteMatched, this);
		},

		_handleRouteMatched: function(oEvent) {
			this.getView().byId("formlistPage").setTitle(oEvent.getParameter("arguments")["?query"].folderName);
			/*
			var sFolderId = oEvent.getParameter("arguments").folderId;
			var oData = this.getOwnerComponent().getModel("diagrams").getData();
			var oModel = new sap.ui.model.json.JSONModel();
			for(var i = 0;i < oData.length;i++){
				
				if(oData[i].FolderGuid === sFolderId){
					console.log(oData[i].DiagramLists);
					this.getView().setModel(new sap.ui.model.json.JSONModel(oData[i].DiagramLists),"lists");
				}
			}
			*/
		},
		onNavPress: function(oEvent) {
			this._oRouter.navTo("folder");
		},

		onItemPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("lists");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			this._oRouter.navTo("filtered", {
				diagramId: oItem.Key.DiagramId,
				query: {
					diagramName: oItem.Key.DiagramName
				}
			});
		}
	});
});