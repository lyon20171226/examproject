sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast"
], function(Controller, MessageToast) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Form", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nNext.Flow7.Dashboard.Applicant.view.Form
		 */
		onInit: function() {
			self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			var oEventBus = sap.ui.getCore().getEventBus();
			oEventBus.subscribe("Flow7", "FORM_DATA_CHANGE", this.onFormDataChange, this);
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nNext.Flow7.Dashboard.Applicant.view.Form
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nNext.Flow7.Dashboard.Applicant.view.Form
		 */
		// onAfterRendering: function() {
		// }

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nNext.Flow7.Dashboard.Applicant.view.Form
		 */
		//	onExit: function() {
		//
		//	}

		onFormDataChange: function(sChanel, sEvent, oData) {
			this.getView().setModel(oData, "storedData");
		},

		onFormSubmit: function() {
			var formData = this.getView().getModel("storedData").getJSON();

			var create = function() {
				var def = $.Deferred();

				$.ajax({
					url: "/Flow7Api/api/fdp/m",
					type: "POST",
					data: formData,
					contentType: "application/json",
					success: function() {
						def.resolve();
					},
					fail: function(data) {
						def.reject(data);
					}
				});

				return def.promise();
			};

			var nginStart = function() {
				var def = $.Deferred();

				$.ajax({
					url: "/Flow7Api/api/engine/start",
					type: "POST",
					data: formData,
					contentType: "application/json",
					success: function() {
						def.resolve();
					},
					fail: function(data) {
						def.reject(data);
					}
				});

				return def.promise();
			}

			create().then(nginStart())
				.done(function() {
					self.updatePage();
				})
				.fail(data => console.log(data));
		},

		updatePage: function() {
			MessageToast.show("表單已成功送出, 請至進行中表單查詢");
			this._oRouter.navTo("welcome");
		}
	});

});