sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nnext/iq/Processing/jslib/Formatter"
], function(Controller, Formatter) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.AllForm", {
		oFormatter: Formatter,
		onInit: function() {
			var self = this;
			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			$.ajax("/Flow7Api/api/dashboard/processing")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					self.getView().setModel(oData, "processingLists");
				});
			this._oRouter.getRoute("filtered").attachMatched(this._handleRouteMatched, this);
		},
		_handleRouteMatched: function(oEvent) {
			var diagramId = oEvent.getParameter("arguments").diagramId;
			var list = this.getView().byId("list");
			var binding = list.getBinding("items");
			binding.filter(
				[new sap.ui.model.Filter([new sap.ui.model.Filter("DiagramId", sap.ui.model.FilterOperator.EQ, diagramId)]),
					false
				]);
			this.getView().byId("listPage").setTitle(oEvent.getParameter("arguments")["?query"].diagramName);
		},
		onCreateForm: function(oEvent) {
			this._oRouter.navTo("form");
		},
		onListPress: function(oEvent) {
			var oObject = oEvent.getSource().getBindingContext("processingLists");
			var oItem = oObject.getModel().getProperty(oObject.getPath());

			if (oItem.DiagramName === "費用課付款申請書") {
				this._oRouter.navTo("detail", {
					requisitionId: oItem.RequisitionId,
					query: {
						diagramName: oItem.DiagramName,
						diagramId: oItem.DiagramId
					}
				});
			}
		}
	});
});