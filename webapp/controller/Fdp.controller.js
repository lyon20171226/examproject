sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageToast",
	"nnext/iq/Processing/3rd/underscore"
], function(Controller, MessageToast, underscore) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.Fdp", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf nNext.Flow7.Dashboard.Applicant.controller.view.Fdp
		 */
		onInit: function() {
			var self = this;

			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			this._oRouter.getRoute("form").attachMatched(this._handleRouteMatched, this);
		},

		_handleRouteMatched: function(oEvent) {
			this.loadInitialData();
			this.loadErpData();
		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nNext.Flow7.Dashboard.Applicant.controller.view.Fdp
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nNext.Flow7.Dashboard.Applicant.controller.view.Fdp
		 */
		// onAfterRendering: function() {
		// }

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nNext.Flow7.Dashboard.Applicant.controller.view.Fdp
		 */
		//	onExit: function() {
		//
		//	}
		loadInitialData: function() {
			var oView = this.getView();

			var oModel =
				(new sap.ui.model.json.JSONModel())
				.attachRequestCompleted(function() {
					oView.setModel(oModel, "fdp");
					sap.ui.getCore().setModel(oModel, "fdp");
				})
				.attachPropertyChange(function() {
					sap.ui.getCore().getEventBus().publish("Flow7", "FORM_DATA_CHANGE", oView.getModel("fdp"));
				});
			oModel.loadData("/Flow7Api/api/fdp");
		},

		loadErpData: function() {
			var self = this;

			var oView = this.getView();
			oView.setModel(new sap.ui.model.json.JSONModel("/ErpApi/api/purma"), "purma");
			oView.setModel(new sap.ui.model.json.JSONModel("/ErpApi/api/cmsme"), "cmsme");
			oView.setModel(new sap.ui.model.json.JSONModel("/ErpApi/api/cmsnb"), "cmsnb");
			sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel("/ErpApi/api/purma"), "purma");
			sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel("/ErpApi/api/cmsme"), "cmsme");
			sap.ui.getCore().setModel(new sap.ui.model.json.JSONModel("/ErpApi/api/cmsnb"), "cmsnb");
			console.log(oView);
			new sap.ui.model.json.JSONModel("/ErpApi/api/receipt").attachRequestCompleted(function(oData) {
				oView.setModel(oData.getSource(), "receiptAll");

				self.renderReceipt(oView.getModel("fdp").getProperty("/DocumentCategoryId"));
			});
			//console.log(oView);
		},

		onPurmaRequest: function() {
			if (!this._oPurmaDialog) {
				this._oPurmaDialog = sap.ui.xmlfragment("nnext.iq.Processing.view.Purma", this);
				this._oPurmaDialog.setModel(this.getView().getModel("purma"));
			}

			this._oPurmaDialog.open();
		},
		onCmsmeRequest: function() {
			if (!this._oCmsmeDialog) {
				this._oCmsmeDialog = sap.ui.xmlfragment("nnext.iq.Processing.view.Cmsme", this);
				this._oCmsmeDialog.setModel(this.getView().getModel("cmsme"));
			}

			this._oCmsmeDialog.open();
		},

		onCmsnbRequest: function() {
			if (!this._oCmsnbDialog) {
				this._oCmsnbDialog = sap.ui.xmlfragment("nnext.iq.Processing.view.Cmsnb", this);
				this._oCmsnbDialog.setModel(this.getView().getModel("cmsnb"));
			}

			this._oCmsnbDialog.open();
		},

		updateFdpData: function(sKey, sValue) {
			var oModel = this.getView().getModel("fdp");

			oModel.setProperty("/" + sKey, sValue);
			this.getView().setModel(oModel, "fdp");
		},

		onPayTypeChange: function(oEvent) {
			var oView = this.getView();
			var sSelection = oView.byId("paytype").getSelectedKey();

			switch (sSelection) {
				case "供應商貨款":
				case "一般請款":
				case "暫付款":
				case "零用金":
					oView.byId("paytypeother").setVisible(false);
					// oView.byId("paysupplierid").setEnabled(false);
					break;
				case "其他":
					oView.byId("paytypeother").setVisible(true);
					break;
			}
		},

		updateReceiptData: function() {
			this.updateFdpData("DocumentCategoryId", this.getView().byId("documentcategoryid").getSelectedKey());
			this.updateFdpData("DocumentCategory", this.getView().byId("documentcategoryid").getSelectedItem().getText());
			this.updateFdpData("InvoiceType", this.getView().byId("invoicetype").getSelectedKey());
			this.updateFdpData("TaxationType", this.getView().byId("taxationtype").getSelectedKey());
		},

		onReceiptChange: function(oEvent) {
			this.renderReceipt(parseInt(this.getView().byId("documentcategoryid").getSelectedKey()));
			this.updateReceiptData();
		},

		renderReceipt: function(nReceiptId) {
			var oView = this.getView();
			var oReceipt = oView.getModel("receiptAll").getProperty("/");

			oView.setModel(new sap.ui.model.json.JSONModel(_.findWhere(oReceipt, {
				"ReceiptId": nReceiptId
			})), "receipt");
		},

		updateFee: function() {
			var total = parseFloat(this.getView().byId("notaxamount").getValue().replace(/[^\d\.-]/g, "")) + parseFloat(this.getView().byId(
				"tax").getValue().replace(/[^\d\.-]/g, ""));

			this.updateFdpData("/Total", total)
		}
	});

});